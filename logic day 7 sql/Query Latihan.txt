Create Tabel Pengarang : 

create table tblPengarang (
id int primary key not null,
kd_Pengarang varchar(7) not null,
nama varchar(30) not null,
alamat varchar(80) not null,
kota varchar(15) not null,
kelamin varchar(1) not null
)
============================
Insert Table Pengarang : 

insert into tblPengarang 
(kd_pengarang, nama, alamat, kota, kelamin)
values
('P0001','Ashadi','Jl. Beo 25','Yogya','P')

valuesnya di ganti(insert satu satu) : 
('P0002','Rian','Jl. Solo 123','Yogya','P')
('P0003','Suwadi','Jl. Semangka 13','Bandung','P')
('P0004','Siti','Jl. Durian 15','Solo','W')
('P0005','Amir','Jl. Gajah 33','Kudus','P')
('P0006','Suparman','Jl. Harimau 25','Jakarta','P')
('P0007','Jaja','Jl. Singa 7','Bandung','P')
('P0008','Saman','Jl. Naga 12','Yogya','P')
('P0009','Anwar','Jl. Tidar 6A','Magelang','P')
('P0010','Fatmawati','Jl. Renjana 4','Bogor','W')
============================
Create Table Gaji : 

create table tblPengarang (
id int primary key not null,
kd_Pengarang varchar(7) not null,
nama varchar(30) not null,
gaji decimal(18,4) not null
)
============================
Insert Table Gaji : 

insert into tblGaji 
(kd_pengarang, nama, gaji)
values
(P0002','Rian',600000)

valuesnya diganti (insert satu satu) : 
(P0005','Amir',700000)
(P0004','Siti',500000)
(P0003','Suwadi',1000000)
(P0010','Fatmawati',600000)
(P0008','Saman',750000)
============================
Nomor 1 : 

select count(nama) as Jumlah_Pengarang from tblPengarang
============================
Nomor 2 : 

SELECT kelamin,
count(*) AS Total
FROM tblPengarang
GROUP BY kelamin
============================
Nomor 3 : 

select kota, count(kota) as JumlahKota
from tblPengarang
group by kota
============================
Nomor 4 : 

select kota, count(kota) as Total
from tblPengarang
group by kota
having
count(kota) > 1
============================
Nomor 5 : 

select max(kd_pengarang) as Terbesar,
min(kd_pengarang) as Terkecil
from tblPengarang
============================
Nomor 6 : 

select max(gaji) as GajiTertinggi, min(gaji) as GajiTerendah
from tblGaji
============================
Nomor 7 : 

select gaji
from tblGaji
where gaji > 600000
============================
Nomor 8 : 

select sum(gaji) as JumlahGaji
from tblGaji
============================
Nomor 9 : 

select 
case when sum(b.gaji) is NULL THEN '0'
ELSE sum(b.gaji)
END AS TotalGaji , a.kota
from tblPengarang as a
left join tblGaji as b
on a.kd_pengarang = b.kd_pengarang
group by a.kota
============================
Nomor 10 : 

select * from tblPengarang
where kd_pengarang between 'P0001' AND 'P0006'

Atau

select * from tblPengarang
where kd_pengarang in ('P0001', 'P0002', 'P0003', 'P0004', 'P0005', 'P0006')

============================
Nomor 11 : 

select * from tblPengarang
where kota = 'Yogya' OR kota = 'Solo' OR kota = 'Magelang'
============================
Nomor 12 : 

select * from tblPengarang
where kota != 'Yogya'
============================
Nomor 13 : 

select * from tblPengarang
where
nama like 'A%' AND
nama like '%i' AND
nama like '__a%' AND
nama not like '%n'
============================
Nomor 14 : 

select * from tblPengarang as a
join tblGaji as b
on a.kd_pengarang = b.kd_pengarang
============================
Nomor 15 : 

select a.kota from tblPengarang as a
join tblGaji as b
on a.kd_pengarang = b.kd_pengarang
where gaji < 1000000
group by a.kota
============================
Nomor 16 : 

alter table tblPengarang
alter column kelamin type varchar (10)
============================
Nomor 17 : 

alter table tblPengarang
add Gelar varchar(12)

============================
Nomor 18 : 

Update tblPengarang set
alamat = 'Jl. Cendrawasih 65',
kota = 'Pekanbaru'
where nama = 'Rian'
============================
Nomor 19 : 

create view vwPengarang as
select kd_pengarang, nama, kota
from tblPengarang

select * from vwPengarang